# TeleMail

A minimal sendmail implementation sending mail to a Telegram chat.

## Instalation

### Debian and Ubuntu (and other Debian derivate)

APT repository is available using the gitlab pages. Here's the supported version with the corresponding distribution to use.

| OS                  | Distribution |
|---------------------|--------------|
| Debian 10 Buster    | buster       |
| Debian 11 Bullseye  | bullseye     |
| Debian 12 Bookworm  | bookworm     |
| Ubuntu 18.04 Bionic | bionic       |
| Ubuntu 20.04 Focal  | focal        |
| Ubuntu 22.04 Jammy  | jammy        |

If your distribution is not included take the one closest to it, it will probably work.

```bash
wget -qO- https://neodc.gitlab.io/telemail/gpg.key | sudo tee /etc/apt/trusted.gpg.d/telemail.asc
echo 'deb https://neodc.gitlab.io/telemail <distribution> main' | sudo tee /etc/apt/sources.list.d/telemail.list
sudo apt update
sudo apt install telemail
```

### Manually

1. Get the compiled binary (from the last [release](https://gitlab.com/neodc/telemail/-/releases) or compile it yourself) then put it your path, for exemple `/usr/bin/telemail`.
2. Create a user for Telemail `adduser --quiet --system --group --no-create-home --home /nonexistent telemail`
3. Give the correct right to the executable `chmod 2755 /usr/bin/telemail && chown telemail:telemail /usr/bin/telemail`
4. Create the log folder and give it the correct right `mkdir /var/log/telemail && chmod 775 /var/log/telemail && chown telemail:telemail /var/log/telemail`
5. Create the log file and give it the correct right `touch /var/log/telemail/telemail.log && chmod 664 /var/log/telemail/telemail.log && chown telemail:telemail /var/log/telemail/telemail.log`
6. Create the config file as described bellow and give it the correct right `chown telemail:telemail /etc/telemail/config.toml && chmod 640 /etc/telemail/config.toml`
7. Create a symlink from sendmail to telemail `ln -s /usr/bin/telemail /usr/sbin/sendmail`

## Config

### Telegram token and chat id

You will need a telegram bot token (see [\@BotFather](https://telegram.me/BotFather) on Telegram) and insert it in `/etc/telemail/config.toml` like this:

```toml
telegram_token = "0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
#chat_id = 123456789
```

Then execute `telemail` witout any argument, if no `chat_id` is set it will start replying to any message with the corresponding chat id.

Now that you have your chat_id you can uncomment the chat_id line and put the correct value (without any quote, it should be an i64).

```toml
telegram_token = "0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
chat_id = 123456789
```

You can now test that everything is working by sending a dummy message like this:

```bash
echo -e "To: You\nContent-Type: text/plain\n\nA small test message" | sendmail
```

You will probably still need an implementation of mail, on Ubuntu you can use `bsd-mailx` (`apt install bsd-mailx`).

### Other options

You can change the way Telemail work with some additional configuration options:

#### `storage_mode`

This option control if Telemail should store sent messages in files under `/var/log/telemail/`, 3 values are accepted:

* `storage_mode = "never"`: will not try to store any messages.
* `storage_mode = "on_error"`: will store messages but delete them when the message is sent without error. **This is the default mode.**
* `storage_mode = "always"`: will store messages and never delete them.

#### `hostname`

If this option is set its value will be used in place of the hostname detected from the system.

#### `upload_email`

Select when the email should be uploaded, by default it is uploaded if the body can't be fully included in the message (see `send_body` below).

4 values are accepted:

* `upload_email = "always"`: The email is always uploaded.
* `upload_email = "long_text"`: The email is uploaded if its body can't be fully inserted in a Telegram message or if it's not a plain text body (based on the Content-type header). **This is the default.**
* `upload_email = "not_text"`: The email is uploaded if its body is not a plain text body (based on the Content-type header).
* `upload_email = "never"`: The email is never uploaded.

#### `send_body`

Select when the email body should be sent in the message, by default it is sent if it's a plain text body (based on the Content-type header) and it can fully fit in a Telegram message (see `max_message_length` below).

If the body should be sent and the email should be uploaded the body will be sent in a separated message, else (if the email should not be uploaded) it will be sent in the same message as the headers.

3 values are accepted:

* `send_body = "never"`: The body will never be sent in the message
* `send_body = "short_text"`: The body will be sent if it's a plain text body (based on the Content-type header) and it can fully fit in a Telegram message. **This is the default.**
* `send_body = "all_text"`: The body will be sent if it's a plain text body (based on the Content-type header), if it can't fit in a Telegram message it will be cut.

#### `max_message_length`

Allow to reduce the max message size. This will also change the size limit between short text and long text for `upload_email` and `send_body`

This must be in the range 0-4096 (4096 is the max size of a Telegram message).

The default value is 4096 (so no limitation other than the one imposed by Telegram).
