use crate::file_storage::StorageMode;
use crate::message::MessageGenerationMode;
use serde::Deserialize;
use std::fs::File;
use std::io::Read;
use telbot_ureq::polling::Polling;
use telbot_ureq::Api;

#[derive(Deserialize, Debug)]
struct RawConfig {
	pub(crate) telegram_token: String,
	pub(crate) chat_id: Option<i64>,
	#[serde(default)]
	pub(crate) storage_mode: StorageMode,
	pub(crate) hostname: Option<String>,
	#[serde(flatten)]
	pub(crate) message_generation_mode: MessageGenerationMode,
}

impl RawConfig {
	fn into_config(self) -> ConfigEnum {
		if let Some(chat_id) = self.chat_id {
			ConfigEnum::Normal(Config {
				telegram_token: self.telegram_token,
				chat_id,
				storage_mode: self.storage_mode,
				hostname: self.hostname,
				message_generation_mode: self.message_generation_mode,
			})
		} else {
			ConfigEnum::Minimal(MinimalConfig {
				telegram_token: self.telegram_token,
			})
		}
	}
}

#[derive(Debug)]
pub(crate) struct Config {
	pub(crate) telegram_token: String,
	pub(crate) chat_id: i64,
	pub(crate) storage_mode: StorageMode,
	pub(crate) hostname: Option<String>,
	pub(crate) message_generation_mode: MessageGenerationMode,
}

struct MinimalConfig {
	telegram_token: String,
}

enum ConfigEnum {
	Normal(Config),
	Minimal(MinimalConfig),
}

pub(crate) fn get_config() -> Config {
	let mut config_file = File::open("/etc/telemail/config.toml").unwrap();
	let mut config_file_content = String::new();
	config_file.read_to_string(&mut config_file_content).unwrap();

	let raw_config: RawConfig = toml::from_str(&config_file_content)
		.map_err(|e| e.to_string())
		.expect("Coun't parse config file");

	match raw_config.into_config() {
		ConfigEnum::Normal(config) => config,
		ConfigEnum::Minimal(config) => {
			find_chat_id(config.telegram_token);
		}
	}
}

fn find_chat_id(telegram_token: String) -> ! {
	let api = Api::new(telegram_token);

	println!("No chat id found! Send a message to your bot in the wanted chat and they will respond with the chat id.");

	for update in Polling::new(&api) {
		let update = update.unwrap();
		if let Some(message) = update.kind.message() {
			if message.kind.is_text() {
				println!("chat_id = {}", message.chat.id);
				api.send_json(&message.reply_text(format!("chat_id = {}", message.chat.id))).unwrap();
			}
		}
	}

	std::process::exit(0);
}
