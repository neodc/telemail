use chrono::{DateTime, Local};
use log::warn;
use rand::random;
use serde::Deserialize;
use std::fs::{remove_file, File};
use std::io::{Seek, SeekFrom};
use std::os::unix::prelude::*;
use std::path::{Display, PathBuf};

#[derive(Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "snake_case")]
pub(crate) enum StorageMode {
	Never,
	OnError,
	Always,
}

impl Default for StorageMode {
	fn default() -> Self {
		Self::OnError
	}
}

impl StorageMode {
	pub fn create_file(self, now: &DateTime<Local>) -> StorageFile {
		StorageFile::create(self, now)
	}
}

#[derive(Debug)]
struct StorageFileInner {
	path: PathBuf,
	file: File,
}

impl StorageFileInner {
	fn create(now: &DateTime<Local>) -> Option<Self> {
		let time = now.format("%F_%T");
		let rand: u16 = random();

		let path = PathBuf::from(format!("/var/log/telemail/{time}.{rand:04x}.eml"));

		let file = File::create(&path);

		if let Err(e) = &file {
			warn!("Coun't open file {}: '{:?}', continuing without storage", path.display(), e);
		}

		file.ok().map(|file| Self { path, file })
	}
}

#[derive(Debug)]
pub(crate) struct StorageFile {
	inner: Option<StorageFileInner>,
	mode: StorageMode,
}

impl StorageFile {
	pub fn create(mode: StorageMode, now: &DateTime<Local>) -> Self {
		let inner = match mode {
			StorageMode::Never => None,
			StorageMode::OnError | StorageMode::Always => StorageFileInner::create(now),
		};

		Self { inner, mode }
	}

	pub fn overwrite(&mut self, buf: &[u8]) {
		if let Some(inner) = &mut self.inner {
			if let Err(e) = inner.file.set_len(0) {
				warn!("Coun't truncate file {}: '{:?}', continuing without storage", inner.path.display(), e);
				self.inner = None;
				return;
			}

			if let Err(e) = inner.file.seek(SeekFrom::Start(0)) {
				warn!(
					"Coun't seek to start of file {}: '{:?}', continuing without storage",
					inner.path.display(),
					e
				);
				self.inner = None;
				return;
			}

			if let Err(e) = inner.file.write_all_at(buf, 0) {
				warn!("Coun't write to file {}: '{:?}', continuing without storage", inner.path.display(), e);
				self.inner = None;
			}
		}
	}

	pub fn permanent_path(&self) -> Option<Display> {
		if let StorageMode::Always = self.mode {
			if let Some(inner) = &self.inner {
				return Some(inner.path.display());
			}
		}

		None
	}

	pub fn delete(self) {
		if let StorageMode::Always = self.mode {
			return;
		}

		if let Some(inner) = self.inner {
			if let Err(e) = remove_file(&inner.path) {
				warn!("Coun't delete file {}: '{:?}'", inner.path.display(), e);
			}
		}
	}
}
