/// Taken and simplify from <https://github.com/habitat-sh/habitat/blob/a1b4452a2ef46dfa4cfc461bf4354617005a4d2a/components/core/src/os/net.rs>
use std::io;

/// Returns the fully qualified domain name of the running machine.
pub fn fqdn() -> String {
	dns_lookup::get_hostname()
		.and_then(|hostname| lookup_fqdn(&hostname))
		.expect("fqdn() was unable to lookup the machine fqdn.")
}

/// Returns the fqdn from the provided hostname.
#[rustfmt::skip]
fn lookup_fqdn(hostname: &str) -> io::Result<String> {
	let hints = dns_lookup::AddrInfoHints {
		flags: libc::AI_CANONNAME,
		..dns_lookup::AddrInfoHints::default()
	};

	// If 'hints.flags' includes the AI_CANONNAME flag, then the ai_canonname
	// field of the first of the addrinfo structures in the returned list is set
	// to point to the official name of the host.
	if let Some(Ok(first_result)) = dns_lookup::getaddrinfo(Some(hostname), None, Some(hints))?.next() {
		Ok(first_result.canonname.expect("Some(canonname) if requested"))
	} else {
		Ok(hostname.to_string())
	}
}
