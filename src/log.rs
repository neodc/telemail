use flexi_logger::{DeferredNow, Duplicate, FileSpec, LogSpecification, Logger, TS_DASHES_BLANK_COLONS_DOT_BLANK};
use log::Record;

pub(crate) fn init() {
	Logger::with(LogSpecification::info())
		.log_to_file(
			FileSpec::default()
				.directory("/var/log/telemail")
				.basename("telemail")
				.suppress_timestamp(),
		)
		.append()
		.format_for_files(log_format)
		.duplicate_to_stderr(Duplicate::Warn)
		.start()
		.unwrap();

	log_panics::init();
}

fn log_format(w: &mut dyn std::io::Write, now: &mut DeferredNow, record: &Record) -> Result<(), std::io::Error> {
	write!(
		w,
		"[{}] {}: {}",
		now.format(TS_DASHES_BLANK_COLONS_DOT_BLANK),
		record.level(),
		&record.args()
	)
}
