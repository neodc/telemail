#![warn(clippy::pedantic)]

mod config;
mod file_storage;
mod fqdn;
mod log;
mod message;

use crate::config::Config;
use crate::file_storage::StorageFile;
use ::log::{error, info};
use chrono::{DateTime, Local};
use eml_parser::eml::Eml;
use eml_parser::errors::EmlError;
use eml_parser::EmlParser;
use getargs::{Opt, Options};
use std::fmt::Write;
use std::io::{stdin, Read};
use telbot_ureq::Api;

fn main() {
	log::init();

	let config = config::get_config();

	info!("Starting with args {:?}", std::env::args().collect::<Vec<_>>());

	let now = chrono::Local::now();

	let mut storage_file = config.storage_mode.create_file(&now);

	let email = get_email_body(&now, &mut storage_file);

	let email_parsed = EmlParser::from_string(email.clone()).parse();

	if let Err(e) = &email_parsed {
		error!("Email parse error: {:?}", e);
	}

	let api = Api::new(&config.telegram_token);

	let caption = get_caption(&email_parsed, &storage_file, &config);

	config
		.message_generation_mode
		.send_email(&api, config.chat_id, &now, caption, email_parsed.ok(), email);

	storage_file.delete();

	info!("Message send, exiting…");
}

fn get_email_body(now: &DateTime<Local>, storage_file: &mut StorageFile) -> String {
	let mut email = String::new();

	stdin().read_to_string(&mut email).expect("Can't read stdin");

	storage_file.overwrite(email.as_bytes());

	let email_parsed = EmlParser::from_string(email.clone()).ignore_body().parse();

	if let Ok(email_parsed) = email_parsed {
		if email_parsed.from.is_none() {
			let (user, fullname) = get_from_to_add();

			if let Some(fullname) = fullname {
				email.insert_str(0, &format!("From: {fullname} <{user}>\n"));
			} else {
				email.insert_str(0, &format!("From: {user}\n"));
			}
		}

		if !email_parsed.headers.iter().any(|header| header.name == "Date") {
			email.insert_str(0, &format!("Date: {}\r\n", now.to_rfc2822()));
		}

		storage_file.overwrite(email.as_bytes());
	}

	email
}

fn get_caption(email: &Result<Eml, EmlError>, storage_file: &StorageFile, config: &Config) -> String {
	let hostname = config.hostname.clone().unwrap_or_else(fqdn::fqdn);

	let mut caption = match email {
		Ok(email) => {
			let email_headers = extract_email_header(email);

			format!(
				"{subject}\n\nFrom: {from}\nTo: {to}\nSend from: {hostname}",
				hostname = hostname,
				from = email_headers.0,
				to = email_headers.1,
				subject = email_headers.2,
			)
		}
		Err(e) => {
			format!("Email parse error: {e}\n\nSend from: {hostname}")
		}
	};

	if let Some(path) = storage_file.permanent_path() {
		let _ = write!(caption, "\nStored: {path}");
	}

	caption
}

fn extract_email_header(email: &Eml) -> (String, String, String) {
	(
		email.from.as_ref().map(ToString::to_string).unwrap_or_default(),
		email.to.as_ref().map(ToString::to_string).unwrap_or_default(),
		email.subject.as_ref().map(ToString::to_string).unwrap_or_default(),
	)
}

fn get_from_to_add() -> (String, Option<String>) {
	let mut user = whoami::username();
	let mut fullname: Option<String> = None;

	let args: Vec<_> = std::env::args().skip(1).collect();
	let opts = Options::new(&args);

	while let Some(opt) = opts.next() {
		if let Ok(opt) = opt {
			match opt {
				Opt::Short('f') => {
					user = opts.value_str().unwrap().to_string();
				}
				Opt::Short('F') => {
					fullname = Some(opts.value_str().unwrap().to_string());
				}
				_ => {}
			}
		}
	}

	(user, fullname)
}
