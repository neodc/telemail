use chrono::{DateTime, Local};
use eml_parser::eml::Eml;
use serde::Deserialize;
use std::fmt::Write;
use telbot_ureq::types::file::InputFile;
use telbot_ureq::types::message::{SendDocument, SendMessage};
use telbot_ureq::Api;

#[derive(Deserialize, Debug)]
pub(crate) struct MessageGenerationMode {
	#[serde(default)]
	upload_email: UploadEmail,
	#[serde(default)]
	send_body: SendBody,
	#[serde(default)]
	max_message_length: MaxMessageLength,
}

const MAX_TELEGRAM_MESSAGE_LEN: usize = 4096;

impl MessageGenerationMode {
	pub fn send_email(&self, api: &Api, chat_id: i64, now: &DateTime<Local>, mut caption: String, email_parsed: Option<Eml>, email: String) {
		let message_type = self.identify_message_type(&caption, &email_parsed);

		let should_upload = message_type.should_upload(self.upload_email);
		let should_send_body = message_type.should_send_body(self.send_body);

		if should_upload {
			let file = InputFile {
				name: format!("{}.eml", now.format("%F %H-%M-%S")),
				data: email.into_bytes(),
				mime: "application/octet-stream".to_string(),
			};

			let upload_message = api
				.send_file(&SendDocument::new(chat_id, file).with_caption(caption))
				.expect("Error while sending document to Telegram");

			if should_send_body {
				let max_len = self.max_message_length.0;

				let email_parsed = email_parsed.expect("Can't be none if should send body = true");
				let body = email_parsed.body.unwrap_or_default();

				let body = if body.len() <= max_len {
					body
				} else {
					format!("{1:.*}…", max_len - 1, body)
				};

				if !body.is_empty() {
					api.send_json(&SendMessage::new(chat_id, body).reply_to(upload_message.message_id))
						.expect("Error while sending reply message to Telegram");
				}
			}
		} else {
			if should_send_body {
				caption += "\n\n";

				let max_len = self.max_message_length.0 - caption.len();

				let email_parsed = email_parsed.expect("Can't be none if should send body = true");
				let body = email_parsed.body.unwrap_or_default();

				if body.len() <= max_len {
					caption += &body;
				} else {
					let _ = write!(caption, "{1:.*}…", max_len - 1, body);
				}
			}

			api.send_json(&SendMessage::new(chat_id, caption))
				.expect("Error while sending message to Telegram");
		}
	}

	fn identify_message_type(&self, caption: &str, email_parsed: &Option<Eml>) -> MessageType {
		if let Some(email_parsed) = email_parsed {
			let is_text = email_parsed
				.headers
				.iter()
				.find(|header| header.name.to_ascii_lowercase() == "content-type")
				.map_or(false, |content_type| content_type.value.to_string().contains("text/plain"));

			if !is_text {
				return MessageType::NotText;
			}

			let max_size = if let UploadEmail::Always = self.upload_email {
				self.max_message_length.0
			} else {
				self.max_message_length.0 - caption.len()
			};

			let empty = String::new();
			let body = email_parsed.body.as_ref().unwrap_or(&empty);

			if body.len() >= max_size {
				MessageType::Long
			} else {
				MessageType::Short
			}
		} else {
			MessageType::NotText
		}
	}
}

#[derive(Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "snake_case")]
enum UploadEmail {
	Never,
	NotText,
	LongText,
	Always,
}

impl Default for UploadEmail {
	fn default() -> Self {
		Self::LongText
	}
}

#[derive(Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "snake_case")]
enum SendBody {
	Never,
	ShortText,
	AllText,
}

impl Default for SendBody {
	fn default() -> Self {
		Self::ShortText
	}
}

#[derive(Deserialize, Debug, Clone, Copy)]
#[serde(try_from = "usize")]
struct MaxMessageLength(usize);

impl TryFrom<usize> for MaxMessageLength {
	type Error = &'static str;

	fn try_from(value: usize) -> Result<Self, Self::Error> {
		if value <= MAX_TELEGRAM_MESSAGE_LEN {
			Ok(MaxMessageLength(value))
		} else {
			Err("max_body_length can't be bigger than 4096")
		}
	}
}

impl Default for MaxMessageLength {
	fn default() -> Self {
		Self(MAX_TELEGRAM_MESSAGE_LEN)
	}
}

#[derive(Debug, Clone, Copy)]
enum MessageType {
	NotText,
	Long,
	Short,
}

impl MessageType {
	fn should_upload(self, upload: UploadEmail) -> bool {
		#[allow(clippy::match_same_arms)]
		match (self, upload) {
			(_, UploadEmail::Never) => false,
			(_, UploadEmail::Always) => true,
			(Self::NotText, _) => true,
			(Self::Long, UploadEmail::LongText) => true,
			(Self::Long, UploadEmail::NotText) => false,
			(Self::Short, _) => false,
		}
	}

	fn should_send_body(self, body: SendBody) -> bool {
		#[allow(clippy::match_same_arms)]
		match (self, body) {
			(_, SendBody::Never) => false,
			(Self::NotText, _) => false,
			(_, SendBody::AllText) => true,
			(Self::Short, SendBody::ShortText) => true,
			(Self::Long, SendBody::ShortText) => false,
		}
	}
}
